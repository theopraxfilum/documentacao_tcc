\section{DIFRAÇÃO ÓTICA}
%O texto adiante retrata sobre o desenvolvimento de um sistema optoeletrônico para aferir o diâmetro de fios de maneira não invasiva.  O texto foi dividido da seguinte maneira: inicialmente são apresentados dois princípios físicos que servem de base para o desenvolvimento do sistema, em seguida são apresentadas diversas simulações do sistema e todas as melhorias propostas para aumentar a acurácia e por fim são apresentados os resultados dos testes feitos em ambiente relevante e comparados ao dados simulados.      
\subsection{Introdução ao Princípio da Difração}

Um dos fenômenos mais estudados na física relacionado a ondas eletromagnéticas ou mecânicas é o fenômeno da difração. Um dos maiores experimentos da física é baseado no estudo do físico inglês \emph{Thomas Young}, conhecido como o experimento da dupla fenda. A difração é um dos principais argumentos a apoiar a ideia da característica ondulatória da luz.

O estudo aqui apresentado se baseia em dois princípios físicos, o princípio de Huygens-Fresnel, apresentado durantes os anos de 1690, pelo físico \emph{Christian Huygens} e aprimorados por \emph{August-Jean Fresnel}. O outro é o princípio de Babinet, apresentado nos anos de 1800.

\subsubsection{Princípio de Huygens}
O princípio de Huygens-Fresnel é um método usualmente aplicado na solução de problemas relacionados a propagação de ondas. Segundo \cite{Arnt2018}, o princípio de Huygens estipula que qualquer ponto de uma onda pode ser interpretado como uma fonte para uma nova onda. Essa nova onda é chama de onda elementar e ela se expande em todas as direções. O Princípio de Huygens foi demonstrado no experimento onde uma onda planar incide em um plano com uma pequena abertura. As ondas que atravessam a abertura passam a se expandir. A figura \ref{fig:Huygens_fig}, demonstra a ocorrência do fenômeno.    
\begin{figure}[H]
    \centering
    \caption{Representação do principio de Huygens.} 
    \includegraphics[width=\linewidth]{../img/chapters/tecnicas/difracao/hyugens.png}
    \\
    Fonte: \cite[p.421]{Arnt2018}, adaptada. 
    \label{fig:Huygens_fig}
\end{figure}
Em seu estudo apresentado em \cite{light1900}, Fresnel estudava sobre a interferência  entre as ondas e constatou que caso seja desejado encontrar a amplitude da onda em qualquer lugar do espaço (que seja longe da fonte) podemos somar todas as ondas, desde que seja levado em consideração a amplitude e a fase de cada uma delas.  

\subsubsection{Princípio de Babinet}
Segundo \cite{princOptics}, o princípio de Babinet atesta que o padrão de difração gerado por um corpo opaco tem forma idêntica ao gerado por um furo de mesmo tamanho e formato, exceto a intensidade geral do padrão. Foi inicialmente proposto nos anos 1800 pelo físico \emph{Jacques Babinet}. Em outras palavras, o que o princípio de Babinet retrata é: 
\begin{citacao}
Existindo um corpo opaco A, rodeado por uma região transparente, o seu complementar A' é um corpo de mesmo formato, porém transparente na região onde A é opaco e opaco ao seu redor. A soma dos padrões de difração gerados por A e A' deve ser a mesma de caso não existisse nenhum dos corpos. Isso significa que os padrões de difração gerados por A e A' tem amplitude iguais e fases opostas. 
\end{citacao}
A figura \ref{fig:Babinet_fig} representa de forma simplificada distribuição de amplitude entre os padrões de uma fenda e um fio ou haste e a somatória dos dois. 
\begin{figure}[H]
    \centering
    \caption{Representação do princípio de Babinet. }
    \includegraphics[width=\linewidth]{../img/chapters/tecnicas/difracao/babinet.png}
    \\
    Fonte: \cite[p.461]{Arnt2018}
    \label{fig:Babinet_fig}
\end{figure}

Uma fenda (\textit{Slit}) e um fio (\textit{Rod}) podem ser considerados complementares uma vez que a largura da fenda seja igual ao diâmetro do fio. Na figura \ref{fig:Babinet_fig} pode ser visto que os padrões de difração de uma fenda e um fio são complementares em uma análise da amplitude dos sinais, visto que ao se somarem, obtém-se o sinal esperado caso não houvesse nada. 

Entretanto, segundo \citeonline{Arnt2018}, a diferença entre os sinais ocorre apenas em nível de amplitude, ao se analisar pela intensidade dos sinais a curva será essencialmente a mesma. Por esse motivo que visualmente, o padrão de difração gerado por um fio ou por uma fenda são iguais.

O princípio de Babinet é importante pois pode ser usada a mesma teoria aplicado a difração de uma fenda para analisar o padrão de difração gerado por um fio. E assim, por meio do padrão de difração de um fio é possível medir seu diâmetro sem entrar em contato com o mesmo.  

\subsubsection{Padrão de difração gerado por um fio}

Existem diversas formas de se medir o diâmetro de um fio por meio de técnicas ópticas com alta resolução. O método clássico de cálculo do diâmetro de um fio ou largura de uma fenda, utiliza um cálculo que é baseado na difração de \textit{Fraunhofer} \footnote{Físico alemão que viveu entre 1787 e 1826, ficou famoso sobre seus estudos sobre a difração e o espectro da luz solar.}. Durante uma pesquisa preliminar foram encontrados diversos artigos na literatura sobre esse assunto. Alguns deles são os estudos encontrados em \cite{article:02} (2), \cite{article:10} (9), \cite{article:03} (3) e em \cite{article:07}(12), e utilizam o modelo de difração de \emph{Fraunhofer}. Outros trazem algumas mudanças nos sistemas óticos, como os encontrados em \cite{article:01}(1), \cite{article:04} (4), \cite{article:05} (6), \cite{article:06} (6), \cite{article:08} (7). Alguns estudos buscam trazer melhoras nos modelos teóricos disponíveis ou analisar peculiaridades, como os encontrados em \cite{article:09}(8), \cite{article:11} (10). Outros como encontrado em \cite{article:12} (11) usam ferramentas de \gls{PDS} para aumentar a resolução do sistema. O estudo relatado aqui utilizará o sistema com a difração na região de \textit{Fraunhofer} e irá aprimorar as técnicas de \gls{PDS} apresentada na literatura. No quadro \ref{tab:Artigos} pode ser vista uma Comparação entre os conteúdos dos artigos estudados. 

\begin{quadro}[H]
    \centering
    \caption{Revisão literária dos artigos}
    \begin{tabular}{|c|l|}
    \hline
        Artigo &    Conteúdo relevante \\ \hline
        1      &    \begin{tabular}[c]{@{}l@{}}Utiliza a posição dos mínimos para comparar o padrão de difração de fios de \\ mesmo diâmetro e materiais diferentes. Analisa fios de 50 a 50 micrômetros.\end{tabular} \\ \hline
        2      &    \begin{tabular}[c]{@{}l@{}}Utiliza uma câmera para capturar o sinal de difração, apresenta os cálculos \\ dos campos para calcular a equação do padrão de difração.\end{tabular} \\ \hline
        3      &    \begin{tabular}[c]{@{}l@{}}Apresenta o sistema para fio ultra finos, o sistema ótico é o mesmo. \\ Utiliza uma aproximação de Debye e para achar coeficientes do perfil de difração. \\ Não considera a existência de ruído.\end{tabular} \\ \hline
        4      &    \begin{tabular}[c]{@{}l@{}}Apresenta um sistema ótico diferente. O cálculo é feito utilizando\\  a iluminação do fio com luz ondas divergentes esféricas ou por ondas planas.\end{tabular} \\ \hline
        5      &    \begin{tabular}[c]{@{}l@{}}Apresenta um sistema ótico diferente. O cálculo é feito utilizando\\  a iluminação do fio com ondas esférica divergente.\end{tabular} \\ \hline
        6      &    Mesma técnica que o anterior, apresenta outras formas de se achar o diâmetro. \\ \hline
        7      &    \begin{tabular}[c]{@{}l@{}}Apresenta um sistema ótico diferente onde a leitura é feita por \\ um angulo diferente e utiliza dois feixes incidindo no fio.\end{tabular} \\ \hline
        8      &    \begin{tabular}[c]{@{}l@{}}Faz um estudo dos campos para tentar melhorar o modelo do \\ padrão de difração de fios e fendas.\end{tabular} \\ \hline
        9      &    Faz um estudo sobre se utilizar a polarização entre fonte e fio. \\ \hline
        10     &    Estuda a influência da onda refletida pelo objeto cilíndrico. \\ \hline
        11     &    \begin{tabular}[c]{@{}l@{}}Utiliza a \gls{FFT} para se obter dados do padrão de difração. \\ Não utiliza a saturação do sinal. Utiliza uma câmera para obter o sinal.\end{tabular} \\ \hline
        12     &    \begin{tabular}[c]{@{}l@{}}Faz um estudo comparativo esse o padrão de difração teórico e o \\ sinal real amostrado por uma CCD.\end{tabular} \\ \hline
    \end{tabular}
    \label{tab:Artigos}
\end{quadro}



É importante ressaltar que toda a análise aqui presente considera que plano onde o perfil de difração é visualizado está na região de \emph{Fraunhofer}. De forma simples, o deseja-se é que o perfil de difração esteja sendo amostrado a uma distância bem maior (cerca de 100 vezes) do que o tamanho do objeto difratador, também conhecido como região de campo distante. Portanto, a distância entre o fio e o sensor deve ser muito maior que o diâmetro do fio. Caso não seja muito maior, é importante levar em conta as equações de Fresnel da difração. Na figura \ref{fig:fres_frau} pode ser visto como o padrão de difração muda, a medida que se distancia do objeto difratador e passa da região de transição para a região de Fraunhofer.

\begin{figure}[H]
    \centering
    \caption{diferença do padrão de difração na região de Fresnel e de Fraunhofer. }
    \includegraphics[width=120mm]{../img/chapters/tecnicas/difracao/fresnel-and-fraunhofer-diffraction.png}
    \\
    Fonte: Própria
    \label{fig:fres_frau}
\end{figure}

Com base na literatura estudada, o padrão de difração para um fio pode ser representado pela equação \ref{eq:sinc_padrao} encontrada extensivamente na literatura:

\begin{equation}
    I(r,Z)=I_0(r)\times sinc^2(\frac{2DZ}{L\lambda})
    \label{eq:sinc_padrao}
\end{equation}

$I_o(r)$ depende da intensidade da luz que é emitida, e $D$ é o diâmetro do fio, $Z$ é a posição no plano onde se deseja calcular a intensidade da luz e $L$ é a distância entre o Plano e o centro do fio. Considerando a equação supracitada o padrão de difração gerado por um fio pode ser visto na figura \ref{fig:perfildifracaosimples}.   Vale ressaltar que por motivo de visualização o diâmetro do fio está bem maior do que o permitido.

\begin{figure}[H]
    \centering 
    \caption{Padrão de difração simples de um fio}
    \includegraphics[width=\linewidth]{../img/chapters/tecnicas/difracao/Triangulo_difracao.jpg}
    \label{fig:perfildifracaosimples}
    \\
    Fonte: Própria.
\end{figure}

A forma mais direta de se obter o diâmetro do fio seria pelo ângulo de difração. Na figura \ref{fig:perfildifracaosimples} o ângulo de difração está indicado pela letra Grega \textit{theta}.  O ângulo de difração é formado posição do ponto de máxima, a posição do primeiro ponto de mínima e a posição do fio. 
Desta forma, para se obter o diâmetro basta utilizar a equação \ref{eq:perfil}, encontrada em \citeonline{Arnt2018}, \citeonline{article:02} e \citeonline{article:08}:

\begin{equation}
    D=\frac{m\lambda}{sen(\theta )}
    \label{eq:perfil}
\end{equation}

Onde $m$ é a ordem da difração (que vale 1 para o primeiro mínimo), $\lambda$ é o comprimento da onda do laser incidente no fio e $\theta$ é o próprio ângulo de difração. Pode-se utilizar da propriedade trigonométrica e transformar o seno do ângulo de difração, na razão entre a distância do máximo ao primeiro mínimo (AC) e distância entre o fio e o ponto de mínimo (ÊC). Utilizando Pitágoras, podemos expressar a reta ÊC como função de L e m1, dessa forma, o seno do angulo de difração pode ser expresso por:

\begin{equation}
    sen(\theta)=\frac{m_1}{\sqrt{m_1^{2}+L^{2}}}
    \label{eq:seno_theta}
\end{equation}

Desta forma, pode-se unir as duas equações \ref{eq:perfil} e \ref{eq:seno_theta}, e assim o diâmetro pode ser calculado de forma mais direta por:
\begin{equation}
    D=\frac{m\lambda(\sqrt{m_1^{2}+L^{2}})}{m_1}
    \label{eq:Diametro}
\end{equation}

O método de difração de \textit{Fraunhofer} é capaz de aferir o diâmetro de forma bem simples e direta. A única variável do sistema é a posição dos mínimos no padrão de difração, uma vez que não seja considerado a variação na posição do fio ou a variação do comprimento de onda da fonte luminosa. O sistema ótico também não possui complexidade, sendo necessário apenas do laser e de um sensor para medir a intensidade da luz em cada ponto. Na figura \ref{fig:difracaosimples} pode ser visto o sistema ótico.

\begin{figure}[H]
    \centering 
    \caption{Sistema óptico para difração simples}
    \includegraphics[width=\linewidth]{../img/chapters/tecnicas/difracao/difracao_simples.jpg}
    \\
    Fonte: Própria
    \label{fig:difracaosimples}
\end{figure}

O desenvolvimento desse estudo deseja ir além de determinar o diâmetro por meio da posição dos mínimos. O escopo proposto deseja aplicar técnicas de \gls{PDS} para aumentar a resolução do sistema.

\subsection{Técnicas de PDS Aplicadas}

As duas técnicas de \gls{PDS} utilizadas são a \gls{FFT} e a aproximação Gaussiana. 

\subsubsection{Transformada rápida de \textit{Fourier}}

A transformada rápida de Fourier \footnote{1768-1830. Foi um matemático e físico francês. Conhecido pelo método de decomposição de funções periódicas em séries trigonométrica, conhecidas como séries de Fourier} \gls{FFT} (do inglês \textit{Fast Fourier Transform})  é um algoritmo usado para computar a transformada discreta de Fourier. E é considerado um dos 10 algoritmos mais importantes do século XX.
 
Na análise por Fourier,  usualmente um sinal variante nos domínios do tempo ou o espaço é convertido para o domínio da frequência. A \gls{DFT}, segundo \citeonline{pdsLyons}.
\begin{citacao}[english]
{The \gls{DFT} is a mathematical procedure used to determine the harmonic or frequency, content of a discrete signal sequence. Although, for our purposes, a discrete signal sequence is a set of values obtained by periodic sampling of a continuous signal in the time domain.}
\end{citacao}
 
Mesmo o autor se referindo a sinais variantes no tempo em sua descrição da \gls{DFT}, a mesma ferramenta pode ser utilizada para sinais que variam no espaço, como o caso do sinal da difração. 
 
A vantagem de se utilizar a \gls{FFT} no lugar de uma transformada discreta de \textit{Fourier} é a redução na complexidade do algoritmo, e consequentemente, redução no tempo de processamento. Isso se dá, pois, segundo \citeonline{pdsLyons} com a transformada rápida ocorre uma redução da complexidade cálculo, já que da transformada discreta, possui uma complexidade de O($N^2$) para O(Nlog(N)) da \gls{FFT}, onde N é o tamanho dos dados. Um dos  mais usados na implementação da \gls{FFT} é o algoritmos de Cooley-Tukey \footnote{Nomeado em homenagem a J. W. Cooley and John Tukey}. A técnica é baseada na implementação do Radix-2, nela, uma transformada de N pontos é dividida em duas transformadas de N/2 pontos. Onde uma parte é formada pelos itens de ordem par (N[0, 2, 4...]) e outra pelos itens de ordem ímpar (N[1, 3, 5...]). Com essa separação, muitos cálculos que antes seriam redundantes são otimizados, poupando tempo de processamento. Mais informações sobre a \gls{FFT} e a transformada discreta de \textit(Fourier) podem ser encontradas em \citeonline{pdsLyons}. 


\subsubsection{Aproximação gaussiana de curvas}
Segundo \citeonline{gauss2012} 
\begin{citacao}[english]
    {Gaussian functions are suitable for describing many processes in mathematics, science, and engineering, making them very useful in the fields of signal and image processing.} 
\end{citacao}
Em outras palavras, a função Gaussiana pode ser usada para representar vários fenômenos na natureza devido a sua forma e distribuição, sendo utilizada bastante nos campos de processamento de imagem e sinal. A equação \ref{eqn:gausseq} apresenta a função Gaussiana.

\begin{equation}
    Y= A\times e ^{-\frac{(X-\mu)^{2}}{2 \sigma^{2}}}
    \label{eqn:gausseq}
\end{equation}

Onde A é a amplitude, $\mu$ é o centro da curva e $\sigma$ controla a largura da mesma curva. o objetivo é utilizar uma série de pontos e tentar aproximar aos parâmetros que formam uma curva Gaussiana. O método mais comum é o da centroid, ele é baseado na simetria da curva e é muito utilizado para se obter a posição de um pico de forma bem eficiente, buscando o parâmetro $\mu$ . 

O algoritmo mais comum é o método de Caruana, nele é utilizado o fato que de a função Gaussiana é a exponencial de uma função quadrática e é feito o logaritmo natural. Na figura \ref{fig:gaussln} pode ser visto o desenvolvimento do logaritmo natural de uma função Gaussiana.

\begin{figure}[H]
    \centering
    \caption{Logaritmo natural da função Gaussiana}
    \includegraphics[width=90mm]{../img/chapters/tecnicas/difracao/gauss_ln.png}
    \includegraphics[width=130mm]{../img/chapters/tecnicas/difracao/gauss_ln2.png}
    \\
    Fonte: \cite{gauss2012}
    \label{fig:gaussln}
\end{figure}

Assim, a equação Gaussiana não linear se torna uma função linear. Dessa forma, A, $\mu$ e $\sigma$ podem ser obtidos pelo sistema de "a", "b" e "c". Para se obter "a", "b" e "c" é usada uma aproximação de parábola. Outras técnicas de se obter a aproximação Gaussiana podem ser encontradas em \cite{gauss2012}.
